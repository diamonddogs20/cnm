import numpy as np
# from CNMx import adj_mat
# from itertools import permutations
# import sys

# filename = sys.argv[1]
# tags = sys.argv[2]
#
# tag_ids = set()
#
# with open(filename) as dataset_file:
#     dataset_file.readline()
#
#     for line in dataset_file:
#         line = line.split()
#         tag_id = int(line[2])
#
#         if tag_id not in tag_ids:
#             tag_ids.add(tag_id)
#
# n_selected_tag = int(tags)
# selected_tag = set(list(tag_ids)[:n_selected_tag])
# user_tag_map = {}
# # count = 0
#
# with open(filename) as dataset_file:
#     dataset_file.readline()
#
#     for line in dataset_file:
#         line = line.split()
#         tag_id = int(line[2])
#         user_id = int(line[0])
#
#         if tag_id in selected_tag:
#             # count += 1
#
#             if user_id not in user_tag_map:
#                 user_tag_map[user_id] = []
#             else:
#                 user_tag_map[user_id].append(tag_id)
#
# sorted_tag = sorted(list(selected_tag))
# tag_map = {tag_id: idx for tag_id, idx in zip(sorted_tag, range(n_selected_tag))}
# user_tag_map = {user_id: set([tag_map[tid] for tid in tag_id]) for user_id, tag_id in user_tag_map.iteritems()}
# # tag_map = {v: i for (i, v) in zip(range(len(sorted_tag)), sorted_tag)}
# print tag_map
#
# pairs = set()
# for v in user_tag_map.itervalues():
#         for p in permutations(v, 2):
#             pairs.add(p)
#
# print len(pairs)
#
# adj_mat = np.zeros((n_selected_tag, n_selected_tag), dtype=float)
# for p in pairs:
#     adj_mat[p] += 1
#
# print np.diag(adj_mat)

def euclidian_distance(a, b):
    return np.sqrt(np.sum((a.astype(float) - b.astype(float))**2))

def euclidian_distance1(a, b):
    return np.sqrt(np.sum((np.asarray(a) - np.asarray(b))**2))

# for num1, num2 in selected_tag:
#     for p in permutations(num1, num2):
#         print euclidian_distance(adj_mat[num1, :],adj_mat[num2, :])
#         print euclidian_distance1(adj_mat[1, :],adj_mat[2,:])