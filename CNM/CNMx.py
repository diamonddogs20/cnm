import numpy as np
import sys
import functionsx as fun

from euclidistancex import euclidian_distance1, euclidian_distance
from functionsx import is_connected
from itertools import permutations

# from scipy.cluster.hierarchy import dendrogram

filename = sys.argv[1]
# tags = sys.argv[2]
user = sys.argv[2]

# tag_ids = set()
user_ids = set()

with open(filename) as dataset_file:
    dataset_file.readline()

    for line in dataset_file:
        line = line.split()
        # tag_id = int(line[2])
        user_id = int(line[0])

        # if tag_id not in tag_ids:
        if user_id not in user_ids:
            # tag_ids.add(tag_id)
            user_ids.add(user_id)

# n_selected_tag = int(tags)
n_selected_user = int(user)
# selected_tag = set(list(tag_ids)[:n_selected_tag])
selected_user = set(list(user_ids)[:n_selected_user])
user_tag_map = {}
# count = 0

with open(filename) as dataset_file:
    dataset_file.readline()

    for line in dataset_file:
        line = line.split()
        tag_id = int(line[2])
        user_id = int(line[0])

        if user_id in selected_user:
        # if tag_id in selected_tag:
            # count += 1

            # if user_id not in user_tag_map:
            #     user_tag_map[user_id] = []
            # else:
            #     user_tag_map[user_id].append(tag_id)
            if tag_id not in user_tag_map:
                user_tag_map[tag_id] = []
            else:
                user_tag_map[tag_id].append(user_id)

# for k, v in user_tag_map.iteritems():
#     print k, v
# print len(user_tag_map.keys())

sorted_user = sorted(list(selected_user))
# sorted_tag = sorted(list(selected_tag))
user_map = {user_id: idx for user_id, idx in zip(sorted_user, range(n_selected_user))}
# tag_map = {tag_id: idx for tag_id, idx in zip(sorted_tag, range(n_selected_tag))}
user_tag_map = {user_id: set([user_map[tid]])}
# user_tag_map = {user_id: set([tag_map[tid] for tid in tag_id]) for user_id, tag_id in user_tag_map.iteritems()}
# tag_map = {v: i for (i, v) in zip(range(len(sorted_tag)), sorted_tag)}
print tag_map

pairs = set()
for v in user_tag_map.itervalues():
    for p in permutations(v, 2):
        pairs.add(p)

print len(pairs)

adj_mat = np.zeros((n_selected_tag, n_selected_tag))
for p in pairs:
    adj_mat[p] = 1

print np.diag(adj_mat)

# adj_mat = np.zeros((34, 34))
# with open(filename, 'rb') as f:
#     for l in f:
#         pair = l.replace('\n', '').split('\t')
#         pair = (int(pair[0]), int(pair[1]))
#         adj_mat[pair[0]-1, pair[1]-1] += 1


# inisiasi
community = {i: [i] for i in xrange(adj_mat.shape[0])}
m = np.sum(adj_mat) / 2
k = np.sum(adj_mat, axis=1)
print community

for am in adj_mat:
    print am
print m
print k

delta_Q = np.zeros(adj_mat.shape)
for sort_idx, a in np.ndenumerate(adj_mat):
    delta_Q[sort_idx] = 0
    if a > 0:
        delta_Q[sort_idx] = (1 / (2 * m)) - ((k[sort_idx[0]] * k[sort_idx[1]]) / (2 * m) ** 2)

a = k / (2 * m)
current_Q = 0

orig_stdout = sys.stdout
f2 = open('C:\Users\ASUS\PycharmProjects\TA2\Result\Processstep-' + sys.argv[1] + sys.argv[2] + '.txt', 'w+')
sys.stdout = f2

counter = 0
while len(community) > 2:
    # max_idx = np.unravel_index(np.argmax(delta_Q), delta_Q.shape)
    max_idx = fun.unravel(delta_Q, community)
    current_Q += delta_Q[max_idx]

    # gabung i ke j (update)
    c_i = max_idx[0]
    c_j = max_idx[1]

    for c_k in community.iterkeys():
        if not (c_k == c_j or c_k == c_i):
            connected_to_i = is_connected(adj_mat, community[c_i], community[c_k])
            connected_to_j = is_connected(adj_mat, community[c_j], community[c_k])

            if connected_to_i and connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] + delta_Q[c_j, c_k]
            elif connected_to_i:
                delta_Q[c_j, c_k] = delta_Q[c_i, c_k] - 2. * (a[c_j] * a[c_k])
            elif connected_to_j:
                delta_Q[c_j, c_k] = delta_Q[c_j, c_k] - 2. * (a[c_i] * a[c_k])

    # delta_Q[:, c_j] = delta_Q[c_j, :].T
    # delta_Q[c_i, :] = -np.inf
    # delta_Q[:, c_i] = -np.inf

    a[c_j] += a[c_i]
    a[c_i] = 0

    # new_list = []
    # new_list.append(community[c_i])
    # community[c_j].append(community[c_i])
    community[c_j].extend(community[c_i])
    del community[c_i]

    print max_idx,
    print current_Q
    # print community

sys.stdout = orig_stdout
f2.close()

orig_stdout = sys.stdout
f3 = open('C:\Users\ASUS\PycharmProjects\TA2\Result\Community-' + sys.argv[1] + sys.argv[2] + '.txt', 'w+')
sys.stdout = f3

print "Komunitas yang terbentuk: "
print community
# print new_list

sys.stdout = orig_stdout
f3.close()

orig_stdout = sys.stdout
f4 = open('C:\Users\ASUS\PycharmProjects\TA2\Result\Euclidean-' + sys.argv[1] + sys.argv[2] + '.txt', 'w+')
sys.stdout = f4

for com_member in community.itervalues():
    for (num, num1) in permutations(com_member, 2):
        # print(adj_mat[num, :])
        # print(adj_mat[num1, :])
        euclid = euclidian_distance((adj_mat[num, :]), adj_mat[num1, :])
        print("Jarak [" + str(num) + "] ke [" + str(num1) + "] :"), euclid
        # print(euclidian_distance((adj_mat[num, :]), adj_mat[num1, :]))
# print euclidian_distance((adj_mat[0, :]), adj_mat[9, :])
# print euclidian_distance((adj_mat[35, :]), adj_mat[39, :])
# print euclidian_distance((adj_mat[35, :]), adj_mat[39, :])
sys.stdout = orig_stdout
f4.close()
